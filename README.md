Инструкция по обрезке видео
===========================


[![Image](https://sawinternal.blob.core.windows.net/gds-images/support/kbgraphics/public/en-us/download.gif "Download") **program.zip**](https://gitlab.com/AlexOne/video-instructions/raw/master/files/program.zip "Download") – сама программа

[![Image](https://sawinternal.blob.core.windows.net/gds-images/support/kbgraphics/public/en-us/download.gif "Download") **Нарезка видео (Encoded).mp4**](https://gitlab.com/AlexOne/video-instructions/raw/master/files/%D0%9D%D0%B0%D1%80%D0%B5%D0%B7%D0%BA%D0%B0%20%D0%B2%D0%B8%D0%B4%D0%B5%D0%BE%20(Encoded).mp4?inline=false "Download") – инструкция

[![Image](https://sawinternal.blob.core.windows.net/gds-images/support/kbgraphics/public/en-us/download.gif "Download") **Как поделить ролик на куски.mp4**](https://gitlab.com/AlexOne/video-instructions/raw/master/files/%D0%9A%D0%B0%D0%BA_%D0%BF%D0%BE%D0%B4%D0%B5%D0%BB%D0%B8%D1%82%D1%8C_%D1%80%D0%BE%D0%BB%D0%B8%D0%BA_%D0%BD%D0%B0_%D0%BA%D1%83%D1%81%D0%BA%D0%B8.mp4?inline=false "Download") – Как поделить ролик на куски

